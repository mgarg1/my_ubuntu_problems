sudo apt-get install git-core

Steps for setting a main project repository :
--Move to the topmost directory of the project,
1.git init
2.git add .
3.git commit -a // git commit -m "message"
-- type any message and Ctrl-O and Ctrl-X(to save and exit nano)

	Steps for cloning the Project from remote directory :
		--but before that commit all the changes in that remote directory
		1.git clone ssh://mgarg1@bingsuns.binghamton.edu/u0/users/2/mgarg1/sys_pro/PROJ3/PROJ3/.git
		--it will make the copy of all the files and directory into a new directory inside your current directory.


	Steps for making change and save to remote:
		--first add and commit changes in your local copy
		--if your remote destination is set 
		$git push 
		--this will push the changes to the remote git repository
	
	Steps for viewing all the pushes in remote:
		--first add and commit all your changes
		$git pull
		--this will pull all the changes made by other users 
		--now to check changes
		$git status
		$git diff 

	Steps for branching and merging

	--other git commands
	git status --files changed since last commit
	git diff --
	git log --list of all last commits

