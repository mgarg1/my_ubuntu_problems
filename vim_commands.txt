
inside .gvimrc
colorscheme <colorscheme> 

:vsp <filename>        | vertical split filename
:e <filename>		   | change current file in the window with new file name

ConqueTerm         | allow bash to run inside vim or any other commmand


auto-completion
-----------------------
CTRL-x CTRL-n : Word completion – forward
CTRL-x CTRL-p : Word completion – backward
CTRL-x CTRL-l : Line completion – forward
CTRL-x CTRL-f : File Name completion – forward

$ cat ~/.vimrc
set dictionary+=/usr/share/dict/words

CTRL-x CTRL-k : Vim Dictionary word completion 


search better
---------------------------
% : search matching paranthesis
* : search next occurence of this word
n : search next occurence of the searched pattern
N : search prev occurence of the searched pattern
/serach_pattern

find and replace
----------------------------
s/Foo/Bar : replace Foo with Bar
s/Foo/Bar/g : replace all occurence of Foo with Bar on line
%s/Foo/Bar/g : replace all occurence of Foo with Bar in File


command while opening
----------------------------
$ vim +200 my.txt
opens from line number 200
$ vim +/main myprog.c
opens from the main

set line number
--------------------------
-show line number
set nu
-hide line number
set nu!

set tabstop
-------------------------
set ts=8

open file in readonly mode
--------------------------
vim -M <filename>

open file on a particular function in a project
-----------------------------------------------
vim -t main

open multiple files in buffer
-----------------------------
-open new file
:e <filename>
-list all open files
:ls 
-switch between files
:b <filename>
:bnext

man pages of a symbol
---------------------
-go on to that symbol and press shift+k

go to previous position
-----------------------
-press ctrl+o


mapping F8 to save compile and run a cprogram
--------------------------------------------
map <F8> :w <CR> :!gcc % -Wall && ./a.out <CR>


switch between vertical splits
------------------------------
ctrl+w h - for left splits
ctrl+w l - for right splits

see special symbol 
------------------
:set list

