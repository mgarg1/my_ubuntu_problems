1.Installing Lamp seprately

apache,php,mysql can be installed seprately on the machine.
phpmyadmin is a application to administer mysql using browser(GUI) using php nothing else.

While using this installation be aware that the files to be run in server should be in the folder
/var/www and this the default location.You can change this location by configure server to look 
for the file in another location (this is called virutal hosting).
To enable virutal hosting look for confiiguration file in /etc/apache2/sites-available/*default.conf
and edit its DocumentRoot to your desired location.


2.In Xampp server

all the files related to Xampp are located inside /opt/lampp/

to setup virtual host in Xampp look for httpd.conf and uncomment
Include etc/extra/httpd-vhosts.conf to enable virtal hosting.

open /opt/lampp/etc/extra/httpd-vhosts.conf and add one more virtual host by copying the 
previous one mandatory field being DocumentRoot and ServerName

<VirtualHost *:80>
    ServerAdmin admin@mohit.local
    DocumentRoot "/home/mohit/server"
    ServerName mohit.local
    ServerAlias local.mohit
    ErrorLog "logs/mohit.local-error_log"
    CustomLog "logs/mohit.locaL-access_log" common

	<Directory  "/home/mohit/server">
			Options All
			AllowOverride All
			Require all granted  
	</Directory>

</VirtualHost>


and then edit /etc/hosts
 127.0.0.1 mohit.local
